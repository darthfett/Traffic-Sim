﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace TrafficSim
{
    interface IWorld
    {
        void Update(GameTime gameTime);

        void Draw(GameTime gameTime);

        void Initialize();
    }
}
