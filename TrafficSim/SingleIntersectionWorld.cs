﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace TrafficSim
{
    class SingleIntersectionWorld : IWorld
    {
        private Car car;

        public void Update(GameTime gameTime)
        {
            car.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            car.Draw(gameTime);
        }

        public void Initialize()
        {
            car.Initialize();
        }

        public SingleIntersectionWorld(Game game)
        {
            car = new Car(game);
        }
    }
}
