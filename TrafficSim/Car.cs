﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TrafficSim
{
    enum Direction
    {
        North,
        South,
        East,
        West
    };

    class Car: DrawableGameComponent
    {
        public const float MILES_PER_HOUR = 0.18078f;

        public Vector2 Position { get; set; }
        public float Speed { get; set; }
        public Direction Facing { get; set; }

        public Point Size { get; set; }

        SpriteBatch spriteBatch;
        Texture2D rect;

        public override void Update(GameTime gameTime)
        {
            switch (gameTime.TotalGameTime.Seconds % 4)
            {
                case 0:
                    Facing = Direction.East;
                    break;
                case 1:
                    Facing = Direction.South;
                    break;
                case 2:
                    Facing = Direction.West;
                    break;
                case 3:
                    Facing = Direction.North;
                    break;
            }
            switch (Facing)
            {
                case Direction.North:
                    Position -= new Vector2(0, Math.Abs(Speed));
                    break;
                case Direction.South:
                    Position += new Vector2(0, Math.Abs(Speed));
                    break;
                case Direction.East:
                    Position += new Vector2(Math.Abs(Speed), 0);
                    break;
                case Direction.West:
                    Position -= new Vector2(Math.Abs(Speed), 0);
                    break;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            Vector2 TopLeft = Position;
            Point rotatedSize = new Point(Size.Y, Size.X);
            switch (Facing)
            {
                case Direction.North:
                    TopLeft -= new Vector2(0, Size.X);
                    spriteBatch.Draw(rect, TopLeft, new Rectangle(Point.Zero, rotatedSize), Color.Gray);
                    break;
                case Direction.South:
                    TopLeft -= new Vector2(Size.Y, 0);
                    spriteBatch.Draw(rect, TopLeft, new Rectangle(Point.Zero, rotatedSize), Color.Gray);
                    break;
                case Direction.East:
                    spriteBatch.Draw(rect, TopLeft, new Rectangle(Point.Zero, Size), Color.Gray);
                    break;
                case Direction.West:
                    TopLeft -= new Vector2(Size.X, Size.Y);
                    spriteBatch.Draw(rect, TopLeft, new Rectangle(Point.Zero, Size), Color.Gray);
                    break;
            }

            spriteBatch.End();
        }
        
        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(GraphicsDevice);

            rect = new Texture2D(GraphicsDevice, 1, 1);
            rect.SetData(new[] { Color.White });
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
            spriteBatch.Dispose();

            rect.Dispose(); 
        }

        public override void Initialize()
        {
            LoadContent();
        }

        public Car(Game game) : base(game)
        {
            Position = new Vector2(100, 100);
            Speed = 5 * MILES_PER_HOUR;
            Facing = Direction.East;
            Size = new Point(21, 8);
        }
    }
}
